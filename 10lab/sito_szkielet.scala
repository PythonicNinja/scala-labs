import akka.actor._

object Sito {
	case class Number(n: Int) { require(n >= 2) }
	case object EndOfData
	case class Result(res: List[Int])
}

class Sito extends Actor {
	import Sito._

	def withNum(myNum: Int): Receive = {
	}

	def withNext(myNum: Int, next: ActorRef): Receive = {
	}

	def readyToFinish(myNum: Int): Receive = {
	}

	def receive = {
	}
}

class Master extends Actor {
	def receive = {
		case max: Int =>
		case Sito.Result(res) =>
	}
}

object Main {
	def main(args: Array[String]): Unit = {
		(if (args.length == 1) toOptInt(args(0)) else None) match {
			case Some(n) =>
				val actsys = ActorSystem("sito")
				actsys.actorOf(Props[Master]) ! n
			case _ =>
				println("Integer value expected.")
		}
	}

	def toOptInt(el: String): Option[Int] = {
		try { Some(el.toInt) } catch {
			case e: java.lang.NumberFormatException =>
				None
		}
	}
}