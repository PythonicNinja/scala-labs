import akka.actor._

object Sito {

	case class Number(n: Int) { require(n >= 2) }
	case object EndOfData
	case class Result(res: List[Int])
}

class Sito extends Actor {
	import Sito._
	import context._

	def withNum: Receive = {
		case myNum: Int => {
			println(myNum)
			parent ! "END"
			}
		case x => println(x)
	}

	// def withNext(myNum: Int, next: ActorRef): Receive = {
		
	// }

	// def readyToFinish(myNum: Int): Receive = {
	// 	become(parent)
	// }

	def receive = {
		case num: Int => {
			println("Jestem")
			println(num)
			
		}
		case _ => println("Jestem")
	}
}

class Master extends Actor {
	def receive = {
		case max: Int => {
			println(max)
			val first = context.system.actorOf(Props[Sito])
			for (n <- 2 to max) { first ! n }
		}
		case Sito.Result(res) => {
			println(res)
		}
		case _ => context.system.shutdown()
	}
}

object Main {
	def main(args: Array[String]): Unit = {
		(if (args.length == 1) toOptInt(args(0)) else None) match {
			case Some(n) =>
				val actsys = ActorSystem("sito")
				actsys.actorOf(Props[Master]) ! n
				actsys.shutdown()
			case _ =>
				println("Integer value expected.")
		}
	}

	def toOptInt(el: String): Option[Int] = {
		try { Some(el.toInt) } catch {
			case e: java.lang.NumberFormatException =>
				None
		}
	}
}