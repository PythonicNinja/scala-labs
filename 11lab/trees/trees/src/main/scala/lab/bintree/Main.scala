package lab.bintree

import akka.actor._

object Node {
	case class Insert(n: Int)
	case class GetDepth(n: Int = 0)
	case class Depth(n: Int)
}

class Node extends Actor with ActorLogging {
	import Node._
	
	def init: Receive = {
		case Insert(n: Int) =>
			log.info(s"$self: [$n]")
			context.become(leaf(n))
	}

	def leaf(value: Int): Receive = {
		case Insert(n: Int) => {
			if(n>value){
				val right = context.actorOf(Props[Node], "right")
				right ! Insert(n)
				context.become(leafWithRight(value, right))
			}
			else{
				val left = context.actorOf(Props[Node], "left")
				left ! Insert(n)
				context.become(leafWithLeft(value, left))
			}
		}
		case GetDepth(n) => {
			log.info(s"$self:  DEPTH $n")
		}
	}	

	def leafWithLeft(value: Int, left: ActorRef): Receive = {
		case Insert(n: Int) => {
			if(n>value){
				val right = context.actorOf(Props[Node], "right")
				right ! Insert(n)
				context.become(leafWithBoth(value, left, right))
			}
			else{
				left ! Insert(n)
			}
		}
		case GetDepth(n) => {
			context.become(leafWithLeftDepth(left))
			left ! GetDepth(n+1)
		}
	}

	def leafWithRight(value: Int, right: ActorRef): Receive = {
		case Insert(n: Int) => {
			if(n>value){
				right ! Insert(n)
			}
			else{
				val left = context.actorOf(Props[Node], "left")
				left ! Insert(n)
				context.become(leafWithBoth(value, left, right))
			}
		}
		case GetDepth(n) => {
			context.become(leafWithRightDepth(right))
			right ! GetDepth(n+1)
		}
	}

	def leafWithBoth(value: Int, left: ActorRef, right: ActorRef): Receive = {
		case Insert(n: Int) => {
			if(n>value){
				right ! Insert(n)
			}
			else{
				left ! Insert(n)
			}
		}
		case GetDepth(n) => {
			context.become(leafWithBothDepth(left, right))
			left ! GetDepth(n+1)
			right ! GetDepth(n+1)
		}
	}


	def leafWithBothDepth(left: ActorRef, right: ActorRef): Receive = {
		case GetDepth(n) => {
			left ! GetDepth(n+1)
			right ! GetDepth(n+1)
		}
	}

	def leafWithLeftDepth(left: ActorRef): Receive = {
		case GetDepth(n) => {
			left ! GetDepth(n+1)
		}
	}
	
	def leafWithRightDepth(right: ActorRef): Receive = {
		case GetDepth(n) => {
			right ! GetDepth(n+1)
		}
	}

	def receive = init
}

object Main {
	import akka.actor.ActorDSL._
	def main(args: Array[String]): Unit = {
		val system = ActorSystem("system")
		import system.log

		actor(system, "main")(new Act {
			import Node._
			val root = system.actorOf(Props[Node], "root")
			for (n <- List(3, -5, 2, 4, -7, 0, -4, 12, 9, -1, 20)) {
				root ! Insert(n)
			}
			root ! GetDepth()
			system.shutdown()
			become {
				case Depth(n) =>
					log.debug(s"Depth = $n: $self")
					system.shutdown()
			}
		})
	}

}