package lab.links

import uk.co.bigbeeconsultants.http.HttpClient
import uk.co.bigbeeconsultants.http.response.Response
import java.net.URL
import akka.actor._

object Messages {
	case class Link(url: String, depth: Int)
	case class GetRanking(url: String, depth: Int, client: ActorRef)
	case class Get(url: String, level: Int)
	case class Hrefs(url: List[String], level: Int)
	case class Ranking(ranking: List[(String, Int)])
	case class Shutdown
}

class LinkRanker extends Actor {
	import Messages._
	def receive = {
		case Link(url, depth) =>{
			val Controller = context.actorOf(Props[Controller], "controller")
			Controller ! GetRanking(url, depth, sender)
		}
		case Ranking(results) =>{
			println(results.toList)
		}
		case Shutdown =>{
			context.stop(self)
		}
	}
}

class Controller extends Actor {
	import Messages._
	def withDepth(depth: Int, client: ActorRef): Receive = {
		case Hrefs(hrefs, level) =>{
			if(level+1<=depth){
				for(href <- hrefs){
					val Getter = context.actorOf(Props[Getter], "getter")
					Getter ! Get(href, level+1)
				}			
			}
		}
	}

	def receive = {
		case GetRanking(url, depth, client) => {
			val Getter = context.actorOf(Props[Getter], "getter")
			Getter ! Get(url, 1)
			context.become(withDepth(depth, client))
		}
	}	
}

class Getter extends Actor {
	import Messages._
	def receive = {
		case Get(url, level) => {	
			val linkRE = """href=['"](http[s]?://[^'" ]+)""".r
			val httpClient = new HttpClient
	    	val response: Response = httpClient.get(new URL(url))
	    	val links = linkRE findAllIn response.body.asString
	    	println(links.toList)
	    	// sender ! Hrefs(links, level)
		}
	}	
}


object Main {

	def main(args: Array[String]): Unit = {
		val system = ActorSystem("system")
		import Messages._

		val LinkRanker = system.actorOf(Props[LinkRanker], "linkRanker")

	 	LinkRanker ! Link("http://www.wp.pl/", 2)

	}

}