package lab.async

import akka.actor._

case class Result(str: String)
case class Link(url: String, depth: Int)
case class GetRanking(url: String, depth: Int, client: ActorRef)
case class Get(url: String, level: Int)
case class Hrefs(urlsMap: Map[String, Int], level: Int)
case class Ranking(ranking: List[(String, Int)])
case class Shutdown()

class LinkRanker extends Actor {
	def receive = {
		case Link(url, depth) =>{
			val Controller = context.actorOf(Props[Controller], "controller")
			Controller ! GetRanking(url, depth, sender)
		}
		case Ranking(results) =>{
			println(results.toList)
		}
		case Shutdown =>{
			context.stop(self)
		}
	}
}

class Controller extends Actor {

	def withDepthDict(depth: Int, client: ActorRef, dict: Map[String, Int]): Receive = {
		case Hrefs(hrefs, level) => {
			
			
			val merged = dict.toSeq ++ hrefs.toSeq
			val grouped = merged.groupBy(_._1).mapValues(_.map(_._2)).mapValues(_.sum)
			context.become(withDepthDict(depth, client, grouped))
			println(grouped)				
			println(grouped.size)				

			for((key, value) <- hrefs){

				if(level+1<=depth){
					val Getter = context.actorOf(Props[Getter])
					Getter ! Get(key, level+1)
				}			
			}




			// partial results from level to client
			// client ! Hrefs(dict, level)
			
		}
	}

	def withDepth(depth: Int, client: ActorRef): Receive = {
		case Hrefs(hrefs, level) =>{
			context.become(withDepthDict(depth, client, hrefs))
			self ! Hrefs(hrefs, level)
		}
	}

	def receive = {
		case GetRanking(url, depth, client) => {
			val Getter = context.actorOf(Props[Getter])
			Getter ! Get(url, 1)
			context.become(withDepth(depth, client))
		}
	}	
}



class Getter extends Actor {
	import concurrent._
	import concurrent.ExecutionContext.Implicits.global

	def receive = {
		case Get(url, level) =>
			println(url)
			println(level)

			val client: Future[HttpResponse] = new Http(url).get("")
					
			val replyTo = sender
			client onSuccess {
				case HttpResponse(code, body, headers) =>
					
					val linkRE = """href=['"](http[s]?://[^'"/ ]+)""".r
					val links = (linkRE findAllIn body).matchData
					val domains = links.map((x: scala.util.matching.Regex.Match) => x.subgroups(0))
					
					val counts = domains.toList.groupBy(w => w).mapValues(_.size)
					
					replyTo ! Hrefs(counts, level)
					context.stop(self)
			}
	}
}

object Main {
	import akka.actor._
	import akka.actor.ActorDSL._
	def main(args: Array[String]): Unit = {
		val system = ActorSystem("system")


		val LinkRanker = system.actorOf(Props[LinkRanker], "linkRanker")

	 	LinkRanker ! Link("http://www.wp.pl/", 2)


		actor(system, "main")(new Act {
			val getter = system.actorOf(Props[Getter])
			getter ! "http://www.wp.pl"
			become {
				case Result(b) =>
					println(b)
					system.shutdown()
			}
		})
	}
}