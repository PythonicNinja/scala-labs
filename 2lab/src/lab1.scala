def inc1(x: Int) = x + 1
 
inc1(4)
 
val inc2 = (x:Int) => x + 1
 
inc2(4) 
 
def max(x: Int, y: Int) =
  if (x > y) x else y