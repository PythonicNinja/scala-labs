// arytmetyka wymierna - wykorzystanie klas niejawnych
 
class Q(l: Int, m: Int) {
  require(m != 0)
  private val g = gcd(l.abs, m.abs)
  val licz = l / g
  val mian = m / g
 
  def + (that: Q) =
    Q(licz * that.mian + that.licz * mian, mian * that.mian)
 
  def * (that: Q) =
    Q(licz * that.licz, mian * that.mian)
 
  override def toString = licz + "/" + mian
 
  private def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)
}
 
object Q {
  def apply(l: Int, m: Int = 1) = new Q(l,m)
}
 
object Main {
 
  implicit class IntAsQ(n: Int) extends Q(n, 1) {
    def � (m: Int = 2): Q = 
      this * 2
  }
 
  def main(args: Array[String]): Unit = {
    val pi��Dziesi�tych = Q(5, 10)
    println("5/10 * 5 == " + (pi��Dziesi�tych * 5))
    println("5 * 5/10 == " + (5 * pi��Dziesi�tych))
    println("200 >>  ==" + (1 � 2))
  }
}