object worksheet {

def funA = "A qq!"
 
def funB() = "A qq!"
 
funA    // -> OK
funB    // -> OK
funB()  // -> OK

 
// Zagadka: skąd poniższy wynik???
//
funA(2) // -> 'q'

}