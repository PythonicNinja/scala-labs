val l1 = List(1,2,3)
val l2 = List(2,3,4)

def len(n: List[_], wynik:Int=0):Int = {
 if(n == Nil){
 	wynik
 }else{
 	len(n.tail,wynik+1)
 } 
}

def mkFlat[A](ll: List[List[A]], wynik: List[A]=List()):List[A] = {
	if(ll == Nil){
		wynik
	}else{
		mkFlat(ll.tail,wynik++ll.head)
	}
}