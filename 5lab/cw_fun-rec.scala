package lab02

// Rozwiązując poniższe ćwiczenia NIE korzystaj z następujących
// standardowych operacji na listach:
// * map
// * filter
// * ::: (oraz „odmian” typu ++)
// * flatten
// * flatMap
// * reverse (oraz „odmian” tzn. reverseMap, reverse_:::)
// W szczególności nie używaj też konstrukcji for na listach.
//
// Nie używaj też zmiennych (wprowadzanych za pomocą „var”).
//


object Cw {

    // Ćwiczenie 1
    def map[A, B](x: List[A], f: A => B): List[B] = x match{
        case Nil => Nil
        case head :: tail => f(head) :: map(tail,f)
    }

    // Ćwiczenie 2
    def filter[A](x: List[A], f: A => Boolean): List[A] = x match {
        case Nil => Nil
        case head :: tail => 
        if(f(head)) 
            head :: filter(tail,f)     
        else 
            filter(tail,f) 
    }

    // Ćwiczenie 3
    // funkcja "cleanup" powinna usuwać wielokrotne, następujące po sobie
    // wystąpienia tego samego elementu na liście
    // np. cleanup(List(1,1,2,1,3,3)) == List(1,2,1,3)
    def cleanup(x: List[Int]): List[Int] = x match {
        case Nil => Nil
        case h1 :: h2 :: tail => 
        if(h1 == h2) 
            h1 :: cleanup(tail)
        else 
            h1 :: h2 :: cleanup(tail)
    }

    // Ćwiczenie 4
    // funkcja "chop" powinna wycinać „podlistę” zaczynającą się od elementu
    // o numerze "b" i kończącą na elemencie o numerze "e" - przyjmijmy, że
    // pierwszy element listy ma numer 1. Przykład
    // chop(List('a,2,'b,3,'c,4),2,4) == List(2,'b,3)
    def chop[A](x: List[A], b: Int, e: Int): List[A] = x match {
        case Nil => Nil        
        case head :: tail => 
        if(e>1)
            chop(x.dropRight(x.length -e),b,0)
        else{
            if(b>1)
                chop(tail,b-1,e)
            else
                x
        }
    }

    // Ćwiczenie 5
    // funkcja "remEls" powinna usuwać co "k"-ty element listy. Przykład:
    // remEls(List(1,1,2,1,3,3),3) == List(1,1,1,3)
    def remEls[A](x: List[A], k: Int, licznik: Int = 1): List[A] = x match {
        case Nil => Nil
        case head :: tail => 
            if(licznik==k)
                remEls(tail, k, 1)
            else
                head :: remEls(tail, k, licznik+1)
    }


    // Ćwiczenie 6
    // funkcja "rot" powinna przesuwać cyklicznie elementy listy o wartość "k".
    // Przykład:
    // rot(List(1,2,3,4,5,6),3) == List(4,5,6,1,2,3)
    def rot[A](x: List[A], k: Int): List[A] = x match {
        case Nil => Nil
        case head :: tail => 
            if(k == 0) 
                x
            else 
                rot(x.last :: head :: tail.dropRight(1), k-1)
    }

    def main(args: Array[String]): Unit = {
      val list = (1 to 20).toList
      val fun = (n: Int) => n * 2
      val pred = (n: Int) => n % 2 == 0
      println(remEls((0 to 1000).toList, 5))
      
      (
        AssertThat(map(list, fun) == list.map(fun)) and
        AssertThat(filter(list, pred) == list.filter(pred)) and
        AssertThat(cleanup(List(1, 1, 2, 1, 3, 3)) == List(1, 2, 1, 3)) and
        AssertThat(chop(List('a,2,'b,3,'c,4),2,4) == List(2,'b,3)) and
        AssertThat(remEls(List(1,1,2,1,3,3),3) == List(1,1,1,3)) and
        AssertThat(rot(List(1,2,3,4,5,6),3) == List(4,5,6,1,2,3))
      )
      .summary()
    }

}

object AssertThat {
  def apply(cond: => Boolean) = assertThat(cond) match {
    case (ok, err, uni) => new AssertThat(ok, err, uni)
  }

  private def assertThat(cond: => Boolean): (Int, Int, Int) = {
    try {
      assert(cond)
      (1, 0, 0)
    } catch {
      case _: NotImplementedError =>
        (0, 0, 1)
      case _: AssertionError =>
        (0, 1, 0)
    }
  }
}

class AssertThat(val ok: Int, val err: Int, val uni: Int) {
  def and(that: AssertThat): AssertThat =
    new AssertThat(ok + that.ok, err + that.err, uni + that.uni)
  
  def summary(): Unit = {
        println(s"Wyniki sprawdzenia:\n  ${ok} OK,\n  ${err} błędów,\n  ${uni} brak implementacji")
  }
}
