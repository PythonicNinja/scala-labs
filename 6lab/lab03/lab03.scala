package lab03

/*

  W rozwiązaniach poniższych ćwiczeń wykorzystaj załączoną listę „data”.
  Lista ta zawiera informacje na temat „operacji bankowych” – jej elementami
  są pary data - kwota operacji. Kwota ujemna oznacza obciążenie rachunku
  (wydatek).

  W swoim rozwiązaniu nie używaj zmiennych (wprowadzanych za pomocą „var”)
  oraz konstrukcji „while” oraz „foreach”.

  Ćwiczenie 07. Oblicz sumaryczną wysokość wpływów i wydatków dla każdego z miesięcy

  Ćwiczenie 08. Dla każdego dnia, którego data znajduje się w danych oblicz
  „balans konta”, tzn. wysokość sumy na koncie po uwzględnieniu operacji, które
  tego dnia zostały „zaksięgowane”. Załóż, że początkowy stan konta to 2500 zł.

  Ćwiczenie 09. Znajdź miesiąc(e), w którym balans wpływów i pbciążeń rachunku
  był najkorzystniejszy tzn. suma wpływów pomniejszona o sumę obciążeń była
  maksymalna.

  Ćwiczenie 10. Używając trybu znakowego sporządź „wykres miesięcznych balansów
  konta”.

*/

object Main {

  // ([rrrr-mm] , wypływy, obciążenia) List[(String, Double, Double)]
  //Ćwiczenie 07. Oblicz sumaryczną wysokość wpływów i wydatków dla każdego z miesięcy
  def zad07(l: List[(String, Double)]): Map[String, (Double, Double)] = {
    val regex = "^([0-9]{4})-([0-9]{2})-([0-9]{2})$".r
    val months = l.groupBy({ case (s,n) => s match{ case regex(year,month,day) => year + month } })
    
    val x = months mapValues ( v => v.foldLeft((0.0,0.0))({
        case (ostatnie,x) => 
          if(x._2>=0)
            (ostatnie._1 + (x._2).toDouble, ostatnie._2) 
          else
            (ostatnie._1, ostatnie._2 - (x._2).toDouble) 
      })  
    )
    x
  }

  // Ćwiczenie 08. Dla każdego dnia, którego data znajduje się w danych oblicz
  // „balans konta”, tzn. wysokość sumy na koncie po uwzględnieniu operacji, które
  // tego dnia zostały „zaksięgowane”. Załóż, że początkowy stan konta to 2500 zł.
  def zad08(l: List[(String, Double)], poczatek: Double = 2500): List[(String,Double)] = {
    val regex = "^([0-9]{4})-([0-9]{2})-([0-9]{2})$".r
    val months = l.groupBy({ case (s,n) => s match{ case regex(year,month,day) => year+month+day } })

    val x = months mapValues ( v => v.foldLeft(0.0)({
        case (ostatnie,x) => 
            (ostatnie.toDouble + (x._2).toDouble) 
      })+poczatek  
    )
    x.toList
  }

  def zad08_month(l: List[(String, Double)], poczatek: Double = 2500): List[(String,Double)] = {
    val regex = "^([0-9]{4})-([0-9]{2})-([0-9]{2})$".r
    val months = l.groupBy({ case (s,n) => s match{ case regex(year,month,day) => year+month } })

    val x = months mapValues ( v => v.foldLeft(0.0)({
        case (ostatnie,x) => 
            (ostatnie.toDouble + (x._2).toDouble) 
      })+poczatek  
    )
    x.toList
  }


  // Ćwiczenie 09. Znajdź miesiąc(e), w którym balans wpływów i pbciążeń rachunku
  // był najkorzystniejszy tzn. suma wpływów pomniejszona o sumę obciążeń była
  // maksymalna.
  // def zad09(l: List[(String,Double)], poczatek: Double = 2500, tryb: Boolean = true): (Double,List[(String,Double)]) = {
  //   val regex = "^([0-9]{4})-([0-9]{2})-([0-9]{2})$".r
  //   val months = l.groupBy({ case (s,n) => s match { case regex(year,month,day) => year+month }})

  //   val x = months mapValues ( v => v.foldLeft(0.0)({
  //       case (ostatnie,x) => (ostatnie.toDouble+x._2.toDouble)
  //     })
  //   )

  //   val y = x.toList
  //   if(tryb)
  //     y.groupBy(_._2).maxBy(_._1)
  //   else
  //     y.groupBy(_._2).minBy(_._1)
  // }


  // Ćwiczenie 10. Używając trybu znakowego sporządź „wykres miesięcznych balansów
  // konta”.
  def zad10(l: List[(String,Double)], maxCzesci: Int = 10): Unit = {

      l match {
        case Nil => Nil
        case head :: tail => 
            if(head._2 == l.minBy(_._2)._2)
              println("=")
            else if(head._2 == l.maxBy(_._2)._2)
              println("="*maxCzesci)
            else{
              println("="*(maxCzesci.toDouble * head._2.toDouble/l.maxBy(_._2)._2.toDouble ).toInt )
            }
            zad10(tail,maxCzesci)
      }      
  }




  def main(args: Array[String]): Unit = {
    val source = scala.io.Source.fromFile("account.txt")
    val regex =  "^([^,]+),(.+)$".r
    val data = source.getLines.toList map {
      case regex(date, amount) => (date, amount.toDouble)
    }
    source.close()
    // data: List[(String, Double)] zawiera dane na temat
    // operacji na koncie.

    // println(zad07(data))
    // println(zad08(data))
    // println(zad09(data))
    var sumaMiesieczna = zad08_month(data,poczatek=0)
    
    println(sumaMiesieczna)
    zad10(sumaMiesieczna)
  }

}
