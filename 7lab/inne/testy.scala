package testy



object testowy {

    class Point(val x: Int, val y: Int) {
        override def toString = f"${x}<->${y}"
        override def equals(other: Any) = other match {
            case that: Point => this.x == that.x && this.y == that.y
            case _ => false
        }
        override def hashCode = 1
    }

    def main(args: Array[String]): Unit = {

        val p1, p2 = new Point(1,2)
        val p3 = new Point(1,3)
    
        println(p1)    
        println(p1 equals p2)
        println(p1 equals p3)
         
        // Równość wartości „==” odwołuje się do „equals”:
        println(p1 == p2)
        println(p1 == p3)
         
        // Problem ze strukturami haszowymi:
        println(scala.collection.immutable.HashSet(p1))
        println(scala.collection.immutable.HashSet(p1) contains p1)
        println(scala.collection.immutable.HashSet(p1) contains p2)

    }

}

