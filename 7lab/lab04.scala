import scala.xml._
import java.net._
import scala.io.Source
import scala.xml.XML._
import scala.xml.dtd.{DocType, PublicID} 


/*
  Korzystając z wiadomości z wykładu oraz przykładów zawartych
  w plikach:

  - kursy.scala
  - toXHTML.scala
  
  napisz program, który pobiera zawartość kanału RSS zawierającego
  aktualne kursy walut (patrz kursy.scala) i produkuje (poprawny!)
  plik XHTML prezentujący uzyskane dane w formie tabelki

  -----------------------------
  | waluta        | kurs      |
  -----------------------------
  | 1 USD         | 2.95 zł   |
  -----------------------------
  ...

*/

object Cw {
  def main(args: Array[String]): Unit = {
        val theUrl = "http://waluty.com.pl/rss/?mode=kursy"
        val xmlString = Source.fromURL(new URL(theUrl)).mkString
        val xml = XML.loadString(xmlString)      
        val doctype = DocType("html", PublicID("-//W3C//DTD XHTML 1.0 Strict//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"), Nil)
        val titles = (xml \\ "item" \ "title")

        val htmlNode =
        <html>
          <head><title>Test</title></head>
          <body>
            <h1>Aktualne kursy walut:</h1>
            <table border="1">
            {for(t <- titles) yield <tr><td>{t.text.split("-")(0)}</td><td>{t.text.split("-")(1)}</td></tr>}
            </table>
          </body>
        </html>

        save("wynik.html", htmlNode, "UTF-8", false, doctype)
  }
}
