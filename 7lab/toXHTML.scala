import scala.xml.XML._
import scala.xml.dtd.{DocType, PublicID} 
object ToXHTML {
  val doctype = DocType("html", PublicID("-//W3C//DTD XHTML 1.0 Strict//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"), Nil)

    def main(args: Array[String]): Unit = {
      val words = List("jeden", "drugi", "trzeci")
      val scalaHome = "http://www.scala-lang.org/"
      val htmlNode =
        <html>
          <head><title>Test</title></head>
          <body>
            <p>Język <a href={scalaHome}>Scala</a> to jest to!</p>
            <h1>Kilka argumentów za:</h1>
            <ul>{words.map(w => <li>{w}</li>)}</ul> 
          </body>
        </html>
      save("wynik.html", htmlNode, "UTF-8", false, doctype)
    }
}
