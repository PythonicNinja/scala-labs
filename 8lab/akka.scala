import akka.actor.{Actor, ActorSystem, Props}

class Pinger extends Actor {
	
	def receive = {
		case "Pong" => println("i dwa"); sender ! "Ping"
	}
}

class Ponger extends Actor {
	
	def receive = {
		case Init => pinger ! "Pong"
		case "Ping" => println("i raz"); sender ! "Pong"
	}
}

object PingPong extends App {
	val system = ActorSystem()
	val pinger = system.actorOf(Props[Pinger])
	val ponger = system.actorOf(Props[Ponger])
	// pinger.tell("Pong", ponger)
	Thread.sleep(1000)
	system.shutdown
}