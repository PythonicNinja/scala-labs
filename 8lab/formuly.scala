sealed abstract class Frm
case object False extends Frm
case object True extends Frm
case class Prop(name: String) extends Frm {
    override def toString = name
}
case class Not(f: Frm) extends Frm {
    override def toString = s"!($f)"
}
case class And(f1: Frm, f2: Frm) extends Frm {
    override def toString = s"($f1 & $f2)"
}
case class Or(f1: Frm,f2: Frm) extends Frm {
    override def toString = s"($f1 | $f2)"
}
case class Imp(f1: Frm, f2: Frm) extends Frm {
    override def toString = s"($f1 -> $f2)"
}
// Jak można spowodować, żeby toString minimalizowało
// liczbę nawiasów?


object formuly{
	
	def main(args: Array[String]): Unit = {
	  Imp(Or(Prop("p"), Prop("q")), Not(And(Prop("q"), Prop("p"))))

		// (p | q) -> !(p & q)

		// Ćwiczenie: napisz funkcję wyliczającą dla danej

		// formuły f zbiór jej wszystkich „podformuł”
		def closure(f: Frm): Set[Frm] = ???

		// np. dla formuły powyżej
		// closure(f) = {p, q, p | q, p & q, !(p & q), f}
		// Jak widać closure(f) zawiera również formułę f.

	}
	
}
