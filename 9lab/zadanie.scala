import akka.actor._

case class Number(n: Int)
case object PrintResult

class Sito extends Actor {
	var myNum: Option[Int] = None  // myNum.isEmpty 
	var next: Option[ActorRef] = None

	def receive = {
		case Number(n) => {
			if(myNum.isEmpty){
				myNum = Some(n)	
			}
			else if(n%myNum.getOrElse(0)!=0){
				if(next.isEmpty)
					next = Some(context.actorOf(Props[Sito]))
				
				for(actor <- next){
					actor ! Number(n)
				}

			}
		}
		case PrintResult => {
			for(n <- myNum){
				println(n)
				for( x <- next){
					x ! PrintResult
				}
			}
		}
	}
}

object Main {
	def main(args: Array[String]): Unit = {
		val system = ActorSystem("system")
		val first = system.actorOf(Props[Sito])
		for (n <- 2 to args(0).toInt) { first ! Number(n) }
		first ! PrintResult
		system.shutdown()
	}
}